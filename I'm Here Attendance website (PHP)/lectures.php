<?php
include "header.php"
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3>Lectures Scheduling</h3>
                </div>
                <div class="col-md-12">
                    <a class="btn btn-success float-right" href="#">Add New Lecture</a>
                    <br>
                    <br>
                    <a class="btn btn-success float-right" href="#">Add an exception dates</a>
                </div>

                <div class="col-md-12">
                    <h3>Group 109 Schedule (Class rom)</h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>CRN</th>
                                <th>Course Title</th>
                                <th>Group</th>
                                <th>Day</th>
                                <th>Time</th>
                                <th>Instructor Name</th>
                                <th>Modify</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>210</td>
                                <td>Programming Language</td>
                                <td>2</td>
                                <td>Sunday</td>
                                <td>9:00 AM - 11:00 AM</td>
                                <td>Dr. Altahir</td>
                                <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                <td><a href="#"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            <tr>
                                <td>210</td>
                                <td>Programming Language</td>
                                <td>2</td>
                                <td>Sunday</td>
                                <td>9:00 AM - 11:00 AM</td>
                                <td>Dr. Altahir</td>
                                <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                <td><a href="#"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            <tr>
                                <td>210</td>
                                <td>Programming Language</td>
                                <td>2</td>
                                <td>Sunday</td>
                                <td>9:00 AM - 11:00 AM</td>
                                <td>Dr. Altahir</td>
                                <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                <td><a href="#"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            <tr>
                                <td>210</td>
                                <td>Programming Language</td>
                                <td>2</td>
                                <td>Sunday</td>
                                <td>9:00 AM - 11:00 AM</td>
                                <td>Dr. Altahir</td>
                                <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                <td><a href="#"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            <tr>
                                <td>210</td>
                                <td>Programming Language</td>
                                <td>2</td>
                                <td>Sunday</td>
                                <td>9:00 AM - 11:00 AM</td>
                                <td>Dr. Altahir</td>
                                <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                <td><a href="#"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
