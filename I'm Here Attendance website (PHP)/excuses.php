<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/ExcFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3>Excuses</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="newExcuse.php" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Excuse</a>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Student ID</th>
                                    <th>Excuse ID</th>
                                    <th>Excuse Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $result = getAllExcusesByStudentID($_SESSION['user_account_session']['record']);
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        $dayofweek = date('l', strtotime($item['excusedate']));
                                        ?>
                                        <tr>
                                            <td><?php echo $item['student_id']; ?></td>
                                            <td><a href="excuse-details.php?i=<?php echo $item['id']?>"><?php echo $item['excuse_id']; ?></a></td>
                                            <td><?php echo $dayofweek.' - '.$item['excusedate']?></td>
                                        </tr>
                                        <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
