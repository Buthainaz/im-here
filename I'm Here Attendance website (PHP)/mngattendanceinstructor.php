<?php
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/AttFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                </div>
                <div class="col-md-12">
                    <h3>Attendance Roster</h3>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Course Title</th>
                                    <th>Group Number</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $result = getAllGroupsByInstructorID($_SESSION['user_account_session']['record']);
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item['course']['activity']; ?></td>
                                            <td><a href="inAttendance-schedule.php?i=<?php echo $item['group']['id'];?>"><?php echo $item['course']['name']; ?></a> </td>
                                            <td><?php echo $item['group']['name']; ?></td>
                                        </tr>
                                        <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>