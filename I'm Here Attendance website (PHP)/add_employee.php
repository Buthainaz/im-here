<?php
include "header.php"
?>
<section class="container">
    <div class="row">
        <div class="col-md-12 employee-section">
            <h3><span>Add New Employee</span></h3>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <form method="post">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="second_name">Middle Name</label>
                                    <input type="text" class="form-control" name="second_name" id="second_name" placeholder="Middle Name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="third_name">Third Name</label>
                                    <input type="text" class="form-control" name="third_name" id="third_name" placeholder="Third Name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="phone2">Phone 2</label>
                                    <input type="text" class="form-control" name="phone2" id="phone2" placeholder="Phone 2">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="office_no">Office No.</label>
                                    <input type="text" class="form-control" name="office_no" id="office_no" placeholder="Office No.">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="employee_coordinator">Employee Coordinator</label>
                                    <input type="text" class="form-control" name="employee_coordinator" id="employee_coordinator" placeholder="Employee Coordinator">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="employee_rank">Employee Rank</label>
                                    <input type="text" class="form-control" name="employee_rank" id="employee_rank" placeholder="Employee Rank">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="employee_dep">Employee Department</label>
                                    <input type="text" class="form-control" name="employee_dep" id="employee_dep" placeholder="Employee Department">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Add New Employee</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
