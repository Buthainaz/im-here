<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: ../attendance.php');
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/AttFunctions.php");

$group = getCourseByGroupId ($_GET['i']);
$lectures = getAllLectureDaysByGroupID($_GET['i']);
$array_result = array();
if(!empty($lectures)){
    foreach ($lectures as $lecture){
        if(!empty($lecture['days'])){
            foreach ($lecture['days'] as $lec){
                $res = getWeekdayInRange($lec,$lecture['semester']['dfrom']->format('Y-m-d'),$lecture['semester']['dto']->format('Y-m-d'));
                $array_result = array_merge($array_result,$res);
            }
        }
    }
    // sort days
    usort($array_result, "date_sort");
    $result = null;
    if(!empty($array_result)){
        foreach ($array_result as $item){
            $att = getAttendanceByGroupStudentId($_SESSION['user_account_session']['record'],$_GET['i'],$item);
            $result[] = array('att'=>$att,'date'=>$item);
        }
    }
}?>
<style>
    .px{
        text-align: center;
        margin-bottom: 5px;
    }
    .px a{
        font-size: 25px;
    }
</style>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3><?php echo $group['course']['name'].' - '.$group['group']['name']?></h3>
                </div>
                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Attendance Date</th>
                                    <th>Attendance Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($result)){
                                        foreach ($result as $item){
                                            $field = '';
                                            if($item['att'][0] == true){
                                                $field = '<input class="form-check-input" type="checkbox" name="attd[]" value="'.$item['date'].'" disabled>';
                                            }else{
                                                $field = '<input class="form-check-input" type="checkbox" name="attd[]" value="'.$item['date'].'" disabled>';
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo $item['date']?>
                                                </td>
                                                <td>
                                                    <?php echo $field?>
                                                </td>
                                            </tr>
                                        <?}
                                    }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
