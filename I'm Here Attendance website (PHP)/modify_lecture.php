<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: semesters.php');
}
if(!isset($_GET['l']) || empty($_GET['l'])){
    header('location: semester-details.php?i='.$_GET['l']);
}
include "header.php";
$pre = 'core/';
include_once ('core/RomFunctions.php');
include_once ('core/LecFunctions.php');
$Lecture = getLectureById($_GET['l']);
?>

<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section pb-0">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/semester-details.php?i=<?php echo $_GET['i']?>"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 0">Modify Lecture</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form method="post" action="controller/lecture/edit.php">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="start_time">Time from</label>
                                    <select class="form-control" name="start_time" id="start_time">
                                        <option value="6" <?php echo ($Lecture['time_from'] == 6 ? "selected":"")?>>6 AM</option>
                                        <option value="7" <?php echo ($Lecture['time_from'] == 7 ? "selected":"")?>>7 AM</option>
                                        <option value="8" <?php echo ($Lecture['time_from'] == 8 ? "selected":"")?>>8 AM</option>
                                        <option value="9" <?php echo ($Lecture['time_from'] == 9 ? "selected":"")?>>9 AM</option>
                                        <option value="10" <?php echo ($Lecture['time_from'] == 10 ? "selected":"")?>>10 AM</option>
                                        <option value="11" <?php echo ($Lecture['time_from'] == 11 ? "selected":"")?>>11 AM</option>
                                        <option value="12" <?php echo ($Lecture['time_from'] == 12 ? "selected":"")?>>12 PM</option>
                                        <option value="13" <?php echo ($Lecture['time_from'] == 13 ? "selected":"")?>>1 PM</option>
                                        <option value="14" <?php echo ($Lecture['time_from'] == 14 ? "selected":"")?>>2 PM</option>
                                        <option value="15" <?php echo ($Lecture['time_from'] == 15 ? "selected":"")?>>3 PM</option>
                                        <option value="16" <?php echo ($Lecture['time_from'] == 16 ? "selected":"")?>>4 PM</option>
                                        <option value="17" <?php echo ($Lecture['time_from'] == 17 ? "selected":"")?>>5 PM</option>
                                        <option value="18" <?php echo ($Lecture['time_from'] == 18 ? "selected":"")?>>6 PM</option>
                                        <option value="19" <?php echo ($Lecture['time_from'] == 19 ? "selected":"")?>>7 PM</option>
                                        <option value="20" <?php echo ($Lecture['time_from'] == 20 ? "selected":"")?>>8 PM</option>
                                        <option value="21" <?php echo ($Lecture['time_from'] == 21 ? "selected":"")?>>9 PM</option>
                                        <option value="22" <?php echo ($Lecture['time_from'] == 22 ? "selected":"")?>>10 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="room">Classroom Number</label>
                                    <select class="form-control" name="room" id="room">
                                        <option value="">Select Classroom</option>
                                        <?php
                                        $result = DisplayAllRooms();
                                        if(!empty($result)) {
                                            foreach ($result as $item) {
                                                ?>
                                                <option value="<?php echo $item['id'];?>" <?php echo ($Lecture['room_id'] == $item['id'] ? "selected":"")?>><?php echo $item['classroomnumber']; ?></option>
                                                <?
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="end_time">Time to</label>
                                    <select class="form-control" name="end_time" id="end_time">
                                        <option value="6" <?php echo ($Lecture['time_to'] == 6 ? "selected":"")?>>6 AM</option>
                                        <option value="7" <?php echo ($Lecture['time_to'] == 7 ? "selected":"")?>>7 AM</option>
                                        <option value="8" <?php echo ($Lecture['time_to'] == 8 ? "selected":"")?>>8 AM</option>
                                        <option value="9" <?php echo ($Lecture['time_to'] == 9 ? "selected":"")?>>9 AM</option>
                                        <option value="10" <?php echo ($Lecture['time_to'] == 10 ? "selected":"")?>>10 AM</option>
                                        <option value="11" <?php echo ($Lecture['time_to'] == 11 ? "selected":"")?>>11 AM</option>
                                        <option value="12" <?php echo ($Lecture['time_to'] == 12 ? "selected":"")?>>12 PM</option>
                                        <option value="13" <?php echo ($Lecture['time_to'] == 13 ? "selected":"")?>>1 PM</option>
                                        <option value="14" <?php echo ($Lecture['time_to'] == 14 ? "selected":"")?>>2 PM</option>
                                        <option value="15" <?php echo ($Lecture['time_to'] == 15 ? "selected":"")?>>3 PM</option>
                                        <option value="16" <?php echo ($Lecture['time_to'] == 16 ? "selected":"")?>>4 PM</option>
                                        <option value="17" <?php echo ($Lecture['time_to'] == 17 ? "selected":"")?>>5 PM</option>
                                        <option value="18" <?php echo ($Lecture['time_to'] == 18 ? "selected":"")?>>6 PM</option>
                                        <option value="19" <?php echo ($Lecture['time_to'] == 19 ? "selected":"")?>>7 PM</option>
                                        <option value="20" <?php echo ($Lecture['time_to'] == 20 ? "selected":"")?>>8 PM</option>
                                        <option value="21" <?php echo ($Lecture['time_to'] == 21 ? "selected":"")?>>9 PM</option>
                                        <option value="22" <?php echo ($Lecture['time_to'] == 22 ? "selected":"")?>>10 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="group">Group</label>
                                    <select class="form-control" name="group" id="group">
                                        <option value="">Select Group</option>
                                        <?php
                                        $result = DisplayAllGroupsLec();
                                        if(!empty($result)) {
                                            foreach ($result as $item) {
                                                ?>
                                                <option value="<?php echo $item['group']['id'];?>" <?php echo ($Lecture['group_id'] == $item['group']['id'] ? "selected":"")?>><?php echo $item['course']['name'] , " - " , $item['group']['name']; ?></option>
                                                <?
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="semester">Days</label>
                                    <select class="form-control" name="day" id="day">
                                        <option value="sunday" <?php echo ($Lecture['day'] == "sunday" ? "selected":"")?>>Sunday</option>
                                        <option value="monday" <?php echo ($Lecture['day'] == "monday" ? "selected":"")?>>Monday</option>
                                        <option value="tuesday" <?php echo ($Lecture['day'] == "tuesday" ? "selected":"")?>>Tuesday</option>
                                        <option value="wednesday" <?php echo ($Lecture['day'] == "wednesday" ? "selected":"")?>>Wednesday</option>
                                        <option value="thursday" <?php echo ($Lecture['day'] == "thursday" ? "selected":"")?>>Thursday</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <input type="hidden" name="semester" value="<?php echo $_GET['i']?>">
                                    <input type="hidden" name="lecture" value="<?php echo $_GET['l']?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save Edits</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 p-0">
                        <?php if(isset($_SESSION['message_session'])){
                            if ($_SESSION['message_session']['status']){?>
                                <div class="col-md-12 alert alert-success">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}else{?>
                                <div class="col-md-12 alert alert-danger">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
