<?php
    session_start();
    if(!isset($_SESSION['user_account_session'])){
        header('location: login.php');
    }
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>
    <title></title>
</head>
<body>
<header class="header">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2 logo">
                <a href="/I'mHere"><img src="assets/icons/logot.png"></a>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-6 text-center" style="margin: auto 0;">
                <?php if ($_SESSION['user_account_session']['type'] == 'admin'){?>
                <nav class="navbar navbar-expand-lg navbar-light w-100">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse w-100" id="navbarSupportedContent">
                        <form class="form-inline w-100" method="get" action="search.php">
                            <div class="form-row align-items-center w-100">
                                <select class="form-control mr-2 col-5" name="filter">
                                    <option value="in">Instructor ID</option>
                                    <option value="st">Student ID</option>
                                    <option value="crn">CRN</option>
                                </select>
                                <input type="text" class="form-control mr-2" name="search_key" id="search_key" placeholder="Search">
                                <button class="btn btn-light my-2 my-sm-0" type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                </nav>
                <?}?>
                <?php if ($_SESSION['user_account_session']['type'] == 'instructor'){?>
                <nav class="navbar navbar-expand-lg navbar-light w-100">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                   
                </nav>
                <?}?>
            </div>
            <div class="col-md-2 m-auto">
                <nav class="navbar navbar-expand-lg navbar-light ml-auto">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo $_SESSION['user_account_session']['username']?>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <?php if ($_SESSION['user_account_session']['type'] == 'admin'){?>
                                    <a class="dropdown-item" href="mngEmployee.php">Manage Employee</a>
                                    <a class="dropdown-item" href="mngStudent.php">Manage Student</a>
                                    <a class="dropdown-item" href="mngAccount.php">Manage Accounts</a>
                                    <a class="dropdown-item" href="mngCourse.php">Manage Courses</a>
                                    <a class="dropdown-item" href="mngGroup.php">Manage Groups</a>
                                    <a class="dropdown-item" href="mngRoom.php">Manage Class Rooms</a>
                                    <?}?>
                                    <a class="dropdown-item" href="resetPassword.php">Change Password</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="controller/user/logout.php"><i class="fa fa-stop"></i> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>