<?php
include "header.php"
?>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <h3>Add New Semester</h3>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form method="post">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="start_date">Date from</label>
                                    <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Select date" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="end_date">Date to</label>
                                    <input type="date" class="form-control" name="end_date" id="end_date" placeholder="Select date" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="semester">Semester</label>
                                    <select class="form-control" required>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="summer">Summer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <input type="text" class="form-control" name="year" id="year" placeholder="E.g. 2019-2020">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Add New Semester</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
