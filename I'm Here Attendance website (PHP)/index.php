<?php
include "header.php"
?>
<?php if ($_SESSION['user_account_session']['type'] == 'student'){ 
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/AttFunctions.php");
include_once ("core/GrpFunctions.php");
?>

    <section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 mt-5">
                    <h3>Attendance Record</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="excuses.php" class="btn btn-dark float-right mb-2"> Excuses for Absence</a>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>CRN</th>
                                    <th>Class Title</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $result = DisplayAllGroupsByStudentId($_SESSION['user_account_session']['record']);
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        
                                        ?>
                                        <tr>
                                            <td width="15%"><a href="sAttendance-schedule.php?i=<?php echo $item['group']['id'];?>"><?php echo $item['course']['crn']; ?></a></td>
                                            <td><?php echo $item['course']['name']; ?></td>
                                            
                                        </tr>
                                        <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    
    
<?php }?>
<?php if ($_SESSION['user_account_session']['type'] == 'instructor'){
    include_once ('mngAttendanceInstructor.php');
}?>
<?php if ($_SESSION['user_account_session']['type'] == 'admin'){?>
    <section class="container">
        <div class="row">
            <div class="col-md-12 section-container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="col-md-12 box-item">
                            <img src="assets/icons/excuses.png">
                            <h3><a href="mngExcuses.php">Excuses Records</a></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12 box-item">
                            <img src="assets/icons/Attendence.png">
                            <h3><a href="attendance.php">Attendance Records</a></h3>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12 box-item">
                            <img src="assets/icons/semesters%20and%20schedules.png">
                            <h3><a href="semesters.php">Semesters & Schedules</a></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?}?>
<?php
include "footer.php"
?>
