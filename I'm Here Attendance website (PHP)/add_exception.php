<?php
include "header.php"
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href=""><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3>Attendance Records</h3>
                </div>
                <div class="container">
                    <div class="row mb-5">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <form class="form-inline" method="post">
                                <div class="form-row">
                                    <div class="form-group mr-5">
                                        <label for="date">Date</label>
                                        <input type="date" class="form-control ml-3" name="date" id="date" placeholder="">
                                    </div>
                                    <div class="form-group mr-5">
                                        <label for="description">Date to</label>
                                        <input type="text" class="form-control ml-3" name="description" id="description" placeholder="">
                                    </div>
                                    <div class="form-group text-center">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>21/12/2202</td>
                                    <td>National Day</td>
                                </tr>
                                <tr>
                                    <td>21/12/2202</td>
                                    <td>National Day</td>
                                </tr>
                                <tr>
                                    <td>21/12/2202</td>
                                    <td>National Day</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
