<?php

if(!isset($_GET['g']) || empty($_GET['g'])){
    header('location: mngGroup.php');
}
include "header.php";
$pre = 'core/';
include_once ('core/GrpFunctions.php');
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section pb-0">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/mngGroupStudent.php?g=<?php echo $_GET['g']?>"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 0">Add New Student to Group</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form method="post" action="controller/group/add_student.php">
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="student">Student</label>
                                    <select class="form-control" name="student" id="student" required>
                                        <?php
                                        $result = DisplayAllStudentsByGroupID($_GET['g']);
                                        if(!empty($result)) {
                                            foreach ($result as $item) {
                                                ?>
                                                <option value="<?php echo $item['studentid'];?>"><?php echo $item['studentfname'].' '.$item['studentmname'].' '.$item['studentlname']; ?></option>
                                                <?
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <input type="hidden" name="group" value="<?php echo $_GET['g']; ?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Add New Student</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 p-0">
                        <?php if(isset($_SESSION['message_session'])){
                            if ($_SESSION['message_session']['status']){?>
                                <div class="col-md-12 alert alert-success">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}else{?>
                                <div class="col-md-12 alert alert-danger">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
unset($_SESSION['message_session']);
include "footer.php"
?>
