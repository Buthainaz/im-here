<?php

if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: semesters.php');
}
include "header.php";
$pre = "core/";
include_once "core/engine.php";
include_once "core/SemFunctions.php";

$result = getSemesterById(intval($_GET['i']));
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section pb-0">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/semesters.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 0">Modify Semester</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form method="post" action="controller/semester/edit.php">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="from">Date From</label>
                                    <input type="date" class="form-control" name="from" id="from" placeholder="Date From" value="<?php echo $result['dfrom']->format('Y-m-d')?>" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="to">Date To</label>
                                    <input type="date" class="form-control" name="to" id="to" placeholder="Date To" value="<?php echo $result['dto']->format('Y-m-d')?>" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="semester">Semester</label>
                                    <select class="form-control" name="semester" id="semester" required>
                                        <option value="1" <?php echo (($result['semester'] == 1) ? "selected":"")?>>1</option>
                                        <option value="2" <?php echo (($result['semester'] == 2) ? "selected":"")?>>2</option>
                                        <option value="summer" <?php echo (($result['semester'] == "summer") ? "selected":"")?>>summer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <input type="text" class="form-control" name="year" id="year" placeholder="Year" value="<?php echo $result['year']?>" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <input type="hidden" name="semester_id" value="<?php echo $_GET['i']?>" required>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save Edits</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 p-0">
                        <?php if(isset($_SESSION['message_session'])){
                            if ($_SESSION['message_session']['status']){?>
                                <div class="col-md-12 alert alert-success">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}else{?>
                                <div class="col-md-12 alert alert-danger">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
unset($_SESSION['message_session']);
include "footer.php"
?>
