<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: attendance.php');
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/AttFunctions.php");
$group = getCourseByGroupId ($_GET['i']);

$lectures = getAllLectureDaysByGroupID($_GET['i']);
$array_result = array();
foreach ($lectures as $lecture){
    foreach ($lecture['days'] as $lec){
        $res = getWeekdayInRange($lec,$lecture['semester']['dfrom']->format('Y-m-d'),date('Y-m-d'));
        $array_result = array_merge($array_result,$res);
    }
}
// sort days
usort($array_result, "date_sort");
?>
<style>
    .px{
        text-align: center;
        margin-bottom: 5px;
    }
    .px a{
        font-size: 25px;
    }
</style>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3><?php echo $group['course']['name'].' - '.$group['group']['name']?></h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if(!empty($array_result)){
                                foreach ($array_result as $item){
                                    $dayofweek = date('l', strtotime($item));?>
                                    <p class="px"><a href="inAttendance-details.php?d=<?php echo $item?>&g=<?php echo $_GET['i']?>"><?php echo $dayofweek.' - '.$item?></a></p>
                                <?}
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
