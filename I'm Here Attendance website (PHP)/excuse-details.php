<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: excuses.php');
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/ExcFunctions.php");

$excuses = getAllExcusesByID(intval($_GET['i']));
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/edu_project"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="container">
                    <div class="row">
                        <?php if($excuses['excuse']['accept_status'] == 0){?>
                        <div class="col-md-12 p-0 mb-2">
                            <a href="accept-excuse.php?i=<?php echo intval($_GET['i'])?>" class="btn btn-success float-right"><i class="fa fa-check"></i> Accept</a>
                            <a href="reject-excuse.php?i=<?php echo intval($_GET['i'])?>" class="btn btn-reject float-right"><i class=""></i> Reject</a>
                        </div>
                        <?php }?>
                        <div class="col-md-12 p-0 mb-2">
                            <?php if(isset($_SESSION['message_session'])){
                                if ($_SESSION['message_session']['status']){?>
                                    <div class="col-md-12 alert alert-success">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}else{?>
                                    <div class="col-md-12 alert alert-danger">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}
                            }?>
                        </div>
                        <div class="col-md-12 p-0">
                            <div class="card text-white">
                                <div class="card-header <?php echo (($excuses['excuse']['accept_status'] == 0) ? 'bg-primary':'bg-success')?> text-center font-weight-bold">Personal Information</div>
                                <div class="card-body">
                                    <div class="row" style="color: #000;">
                                        <div class="col-md-6">
                                            <p><strong>Student Name:</strong> <span><?php echo $excuses['student']['studentfname'].' '. $excuses['student']['studentmname'].' '. $excuses['student']['studentlname'];?></span></p>
                                            <p><strong>ID:</strong> <span><?php echo $excuses['student']['studentid'];?></span></p>
                                            <p><strong>Excuse ID:</strong> <span><?php echo $excuses['excuse']['excuse_id'];?></span></p>
                                        </div>
                                        <div class="col-md-6">
                                            <p><strong>Phone Number:</strong> <span><?php echo $excuses['student']['studentphone'];?></span></p>
                                            <p><strong>Term:</strong> <span> Term <?php echo $excuses['semester']['semester'].' ('.$excuses['semester']['year'].')';?></span></p>
                                            <p><strong>Type Of excuse:</strong> <span><?php echo $excuses['excuse']['excusetype'];?></span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 p-0" style="border: 1px solid #eee;margin-top: 20px;height: 900px">
                            <object data="uploads/<?php echo $excuses['file']['pdf_file']?>" type="application/pdf" width="100%" height="100%"></object>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
