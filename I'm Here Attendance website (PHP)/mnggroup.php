<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/GrpFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Groups</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="newGroup.php" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Group</a>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Group Name</th>
                                    <th>Course Name</th>
                                    <th>Instructor Name</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $result = DisplayAllGroups();
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        ?>
                                        <tr>
                                            <td><a href="mngGroupStudent.php?g=<?php echo $item['group']['id'];?>"><?php echo $item['group']['name']; ?></a> </td>
                                            <td><?php echo $item['course']['name']; ?></td>
                                            <td><?php echo $item['instructor']['emp_fname'].' '.$item['instructor']['emp_mname'].' '.$item['instructor']['emp_lname']; ?></td>
                                            <td><a href="delete_group.php?i=<?php echo $item['group']['id']?>" class="btn btn-danger">DELETE</a></td>
                                        </tr>
                                    <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 p-0">
                            <?php if(isset($_SESSION['message_session'])){
                                if ($_SESSION['message_session']['status']){?>
                                    <div class="col-md-12 alert alert-success">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}else{?>
                                    <div class="col-md-12 alert alert-danger">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
unset($_SESSION['message_session']);
include "footer.php"
?>
