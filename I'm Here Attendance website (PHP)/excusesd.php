<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: semesters.php');
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/ExcFunctions.php");
?>
<style>
    .px{
        text-align: center;
        margin-bottom: 5px;
    }
    .px a{
        font-size: 30px;
    }
</style>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                        <a href="/edu_project/mngExcuses.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3>Excuses Records</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            $excuses = getAllExcusesByDate($_GET['i']);
                            if(!empty($excuses)) {
                                foreach ($excuses as $item) {
                                    ?>
                                        <p class="px">
                                            <a href="excuse-details.php?i=<?php echo $item['excuse']['id']?>"><?php echo $item['student']['studentid'].' - '.$item['excuse']['excuse_id']?></a>
                                        </p>
                                    <?
                                }
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
