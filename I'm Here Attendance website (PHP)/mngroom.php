<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/RomFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Class Rooms</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <a href="newRoom.php" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Class Room</a>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Room Number</th>
                                    <th>Room Type</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $result = DisplayAllRooms();
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item['classroomnumber']; ?></td>
                                            <td><?php echo $item['classroomtype']; ?></td>
                                        </tr>
                                    <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
