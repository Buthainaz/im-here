<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: semesters.php');
}
include "header.php";
$pre = 'core/';
include_once ('core/RomFunctions.php');
include_once ('core/GrpFunctions.php');
?>

<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section pb-0">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/semester-details.php?i=<?php echo $_GET['i']?>"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 0">Lectures Scheduling</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form method="post" action="controller/lecture/add.php">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="start_time">Time from</label>
                                    <select class="form-control" name="start_time" id="start_time">
                                        <option value="6">6 AM</option>
                                        <option value="7">7 AM</option>
                                        <option value="8">8 AM</option>
                                        <option value="9">9 AM</option>
                                        <option value="10">10 AM</option>
                                        <option value="11">11 AM</option>
                                        <option value="12">12 PM</option>
                                        <option value="13">1 PM</option>
                                        <option value="14">2 PM</option>
                                        <option value="15">3 PM</option>
                                        <option value="16">4 PM</option>
                                        <option value="17">5 PM</option>
                                        <option value="18">6 PM</option>
                                        <option value="19">7 PM</option>
                                        <option value="20">8 PM</option>
                                        <option value="21">9 PM</option>
                                        <option value="22">10 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="room">Classroom Number</label>
                                    <select class="form-control" name="room" id="room">
                                        <option value="">Select Classroom</option>
                                        <?php
                                        $result = DisplayAllRooms();
                                        if(!empty($result)) {
                                            foreach ($result as $item) {
                                                ?>
                                                <option value="<?php echo $item['id'];?>"><?php echo $item['classroomnumber']; ?></option>
                                                <?
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="end_time">Time to</label>
                                    <select class="form-control" name="end_time" id="end_time">
                                        <option value="6">6 AM</option>
                                        <option value="7">7 AM</option>
                                        <option value="8">8 AM</option>
                                        <option value="9">9 AM</option>
                                        <option value="10">10 AM</option>
                                        <option value="11">11 AM</option>
                                        <option value="12">12 PM</option>
                                        <option value="13">1 PM</option>
                                        <option value="14">2 PM</option>
                                        <option value="15">3 PM</option>
                                        <option value="16">4 PM</option>
                                        <option value="17">5 PM</option>
                                        <option value="18">6 PM</option>
                                        <option value="19">7 PM</option>
                                        <option value="20">8 PM</option>
                                        <option value="21">9 PM</option>
                                        <option value="22">10 PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="group">Group</label>
                                    <select class="form-control" name="group" id="group">
                                        <option value="">Select Course Group</option>
                                        <?php
                                        $result = DisplayAllGroups();
                                        if(!empty($result)) {
                                            foreach ($result as $item) {
                                                ?>
                                                <option value="<?php echo $item['group']['id'];?>"><?php echo $item['course']['name'] , " - " , $item['group']['name']; ?></option>
                                                <?
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="semester">Days</label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input ml-3" type="checkbox" id="sunday" name="days[]" value="sun">
                                        <label class="form-check-label" for="sun">Sun</label>
                                        <input class="form-check-input ml-3" type="checkbox" id="monday" name="days[]" value="mon">
                                        <label class="form-check-label" for="mon">Mon</label>
                                        <input class="form-check-input ml-3" type="checkbox" id="tuesday" name="days[]" value="tue">
                                        <label class="form-check-label" for="tue">Tue</label>
                                        <input class="form-check-input ml-3" type="checkbox" id="wednesday" name="days[]" value="wed">
                                        <label class="form-check-label" for="thu">Wed</label>
                                        <input class="form-check-input ml-3" type="checkbox" id="thursday" name="days[]" value="thu">
                                        <label class="form-check-label" for="thu">Thu</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <input type="hidden" name="semester" value="<?php echo $_GET['i']?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Add New Lecture</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 p-0">
                        <?php if(isset($_SESSION['message_session'])){
                            if ($_SESSION['message_session']['status']){?>
                                <div class="col-md-12 alert alert-success">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}else{?>
                                <div class="col-md-12 alert alert-danger">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
