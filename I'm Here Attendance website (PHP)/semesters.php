<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/SemFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Semesters</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="newSemester.php" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Semester</a>
                        </div>
                        <div class="col-md-12 mt-4">
                            <?
                            $result = DisplayAllSemesters();
                            if(!empty($result)) {
                                foreach ($result as $item) {
                                    ?>
                                    <h3 class="text-center" style="margin-bottom: 10px;">
                                        <a href="semester-details.php?i=<?php echo $item['id'];?>">
                                            <?php echo $item['year'].' Semester '.$item['semester'];?>
                                        </a>
                                    </h3>
                                    <?
                                }
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
