<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/AttFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3>Attendance Records</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>CRN</th>
                                    <th>Course Title</th>
                                    <th>Group</th>
                                    <th>Instructor Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $result = getAllGroups();
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item['course']['crn']; ?></td>
                                            <td><a href="attendance-schedule.php?i=<?php echo $item['group']['id'];?>"><?php echo $item['course']['name']; ?></a> </td>
                                            <td><?php echo $item['group']['name']; ?></td>
                                            <td><?php echo $item['instructor']['emp_fname'].' '.$item['instructor']['emp_mname'].' '.$item['instructor']['emp_lname']; ?></td>
                                        </tr>
                                        <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
