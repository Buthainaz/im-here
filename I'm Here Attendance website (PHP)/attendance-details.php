<?php
if(!isset($_GET['g']) || empty($_GET['g'])){
    header('location: attendance.php');
}
if(!isset($_GET['d']) || empty($_GET['d'])){
    header('location: attendance-schedule.php?i='.$_GET['g']);
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/AttFunctions.php");
$group = getCourseByGroupId ($_GET['g']);
$attendances = getAttendanceByGroupId($_GET['g'],$_GET['d']);
?>
<style>
    .px{
        text-align: center;
        margin-bottom: 5px;
    }
    .px a{
        font-size: 25px;
    }
</style>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/attendance-schedule.php?i=<?php echo $_GET['g'];?>"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3><?php echo $group['course']['name'].' - '.$group['group']['name']?></h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="controller/attendance/edit.php">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Student Name</th>
                                    <th>Absent Status</th>
                                   <!-- <th>Absent Percentage</th> -->
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                if(!empty($attendances)) {
                                    foreach ($attendances as $item) {
                                        $field = '';
                                        if($item['att']){
                                            $field = '<input class="form-check-input" type="checkbox" enable checked>';
                                        }else{
                                            $field = '<input class="form-check-input" type="checkbox" enable>';
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $item['student']['studentid']; ?></td>
                                            <td><?php echo $item['student']['studentfname'] . ' ' . $item['student']['studentmname'] . ' ' . $item['student']['studentlname']; ?></td>
                                            <td><?php echo $field; ?></td>
                                            <!-- <td></td> -->
                                        </tr>
                                        <?
                                    }
                                }?>
                                </tbody>
                            </table>
                                 <input type="hidden" name="date" id="date" value="<?php echo $_GET['d']?>" required>
                                <input type="hidden" name="group" id="group" value="<?php echo $_GET['g']?>" required>
                             <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                            </div>
                             <div class="col-md-12">
                            <?php if(isset($_SESSION['message_session'])){
                                if ($_SESSION['message_session']['status']){?>
                                    <div class="col-md-12 alert alert-success">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}else{?>
                                    <div class="col-md-12 alert alert-danger">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}
                            }?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
