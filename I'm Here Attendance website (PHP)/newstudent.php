<?php
include "header.php"
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section pb-0">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/mngStudent.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 0">Add New Student</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form method="post" action="controller/student/add.php">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="fname">First Name</label>
                                    <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="mname">Middle Name</label>
                                    <input type="text" class="form-control" name="mname" id="mname" placeholder="Middle Name" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="lname">Last Name</label>
                                    <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <select class="form-control" name="gender" id="gender" required>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="birth_date">Birth Date</label>
                                    <input type="date" class="form-control" name="birth_date" id="birth_date" placeholder="Birth Date">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="blood">Blood Type</label>
                                    <input type="text" class="form-control" name="blood" id="blood" placeholder="Blood Type">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="national_no">National Number</label>
                                    <input type="text" class="form-control" name="national_no" id="national_no" placeholder="National Number">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Add New Student</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 p-0">
                        <?php if(isset($_SESSION['message_session'])){
                            if ($_SESSION['message_session']['status']){?>
                                <div class="col-md-12 alert alert-success">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}else{?>
                                <div class="col-md-12 alert alert-danger">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
unset($_SESSION['message_session']);
include "footer.php"
?>
