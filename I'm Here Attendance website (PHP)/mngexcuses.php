<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/ExcFunctions.php");
?>
<style>
    .px{
        text-align: center;
        margin-bottom: 5px;
    }
    .px a{
        font-size: 30px;
    }
</style>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3>Excuses</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            $excuses = getAllExcusesDate();
                            if(!empty($excuses)) {
                                foreach ($excuses as $item) {
                                    $dayofweek = date('l', strtotime($item['excusedate']));
                                    $date = date('d/m/Y', strtotime($item['excusedate']));
                                    ?>
                                        <p class="px">
                                            <a href="ExcusesD.php?i=<?php echo $item['excusedate']?>"><?php echo $dayofweek.' - '.$date?></a>
                                        </p>
                                    <?
                                }
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
