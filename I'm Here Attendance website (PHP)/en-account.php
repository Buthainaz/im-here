<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: newAccount.php');
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/AccFunctions.php");

$result = getAccountByID(intval($_GET['i']));
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="mngAccount.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Enable Account</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="controller/account/enable.php">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <p class="text-center">Are you sure you want to enable "<?php echo $result['username']?>" account ?</p>
                                        <div class="form-group text-center">
                                            <input type="hidden" class="form-control" name="i" id="i" value="<?php echo $result['id']?>" required>
                                            <button type="submit" class="btn btn-danger">Yes</button>
                                            <a href="mngAccount.php" class="btn btn-warning">No</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
