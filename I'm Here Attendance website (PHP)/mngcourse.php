<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/CrsFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Courses</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="newCourse.php" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Course</a>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Course CRN</th>
                                    <th>Course Code</th>
                                    <th>Course Name</th>
                                    <th>Course Activity</th>
                                    <!--<th></th>-->
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $result = DisplayAllCourses();
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item['crn']; ?></td>
                                            <td><?php echo $item['course_code']; ?></td>
                                            <td><?php echo $item['name']; ?></td>
                                            <td><?php echo $item['activity']; ?></td>
                                        </tr>
                                    <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
