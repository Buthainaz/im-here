<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: semesters.php');
}
if(!isset($_GET['l']) || empty($_GET['l'])) {
    header('location: semester-details.php?i='.$_GET['i']);
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/LecFunctions.php");

$result = getLectureById(intval($_GET['l']));
//var_dump($result);
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="semester-details.php?i=<?php echo $_GET['i']?>"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Delete Lecture</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="controller/lecture/delete.php">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <p class="text-center">Are you sure you want to delete this lecture ?</p>
                                        <div class="form-group text-center">
                                            <input type="hidden" name="l" id="l" value="<?php echo $result['id']?>" required>
                                            <input type="hidden" name="i" id="i" value="<?php echo $_GET['i']?>" required>
                                            <button type="submit" class="btn btn-danger">Yes</button>
                                            <a href="semester-details.php?i=<?php echo $_GET['i']?>" class="btn btn-warning">No</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
