<?php
include "header.php"
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section pb-0">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/semesters.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 0">Add New Semester</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form method="post" action="controller/semester/add.php">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="from">Date From</label>
                                    <input type="date" class="form-control" name="from" id="from" placeholder="Date From" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="to">Date To</label>
                                    <input type="date" class="form-control" name="to" id="to" placeholder="Date To" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="semester">Semester</label>
                                    <select class="form-control" name="semester" id="semester" required>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="summer">summer</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <input type="text" class="form-control" name="year" id="year" placeholder="Year" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Add New Semester</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 p-0">
                        <?php if(isset($_SESSION['message_session'])){
                            if ($_SESSION['message_session']['status']){?>
                                <div class="col-md-12 alert alert-success">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}else{?>
                                <div class="col-md-12 alert alert-danger">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
unset($_SESSION['message_session']);
include "footer.php"
?>
