<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: semesters.php');
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/RomFunctions.php");
include_once ("core/LecFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/semesters.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Lectures Scheduling</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="modSemester.php?i=<?php echo $_GET['i']?>" class="btn btn-success float-right mb-2">Modify Semester</a>
                        </div>
                        <div class="col-md-12">
                            <a href="add_lecture.php?i=<?php echo $_GET['i']?>" class="btn btn-success float-right mb-2">Add New Lecture</a>
                        </div>
                        <div class="col-md-12">
                            <a href="newException.php?i=<?php echo $_GET['i']?>" class="btn btn-success float-right mb-2">Add an Exception dates</a>
                        </div>
                        <div class="col-md-12">
                            <?php if(isset($_SESSION['message_session'])){
                                if ($_SESSION['message_session']['status']){?>
                                    <div class="col-md-12 alert alert-success">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}else{?>
                                    <div class="col-md-12 alert alert-danger">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}
                            }?>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">

                        <div class="col-md-12">
                            <?php
                            $result = DisplayAllRooms();
                            if(!empty($result)) {
                                foreach ($result as $item) {
                                    ?>
                                    <div class="col-md-12">
                                        <h2 class="text-center mb-3 mt-4">
                                            Class <?php echo $item['classroomnumber']; ?> Schedule (<?php echo $item['classroomtype']; ?>)
                                        </h2>
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>CRN</th>
                                                <th>Course Title</th>
                                                <th>Group</th>
                                                <th>Day</th>
                                                <th>Time</th>
                                                <th>Instructor Name</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $result = DisplayAllLectures($_GET['i'],$item['id']);
                                            if(!empty($result)) {
                                                foreach ($result as $item) {
                                                    $from = '';
                                                    $to = '';
                                                    if($item['lecture']['time_from'] > 12){
                                                        $from = ($item['lecture']['time_from'] - 12) . ' PM';
                                                    }else{
                                                        $from = $item['lecture']['time_from'] . ' AM';
                                                    }
                                                    if($item['lecture']['time_to'] > 12){
                                                        $to = ($item['lecture']['time_to'] - 12) . ' PM';
                                                    }else{
                                                        $to = $item['lecture']['time_to'] . ' AM';
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $item['course']['crn']; ?></td>
                                                        <td><?php echo $item['course']['name']; ?></td>
                                                        <td><?php echo $item['group']['name']; ?></td>
                                                        <td><?php echo strtoupper($item['lecture']['day']); ?></td>
                                                        <td><?php echo $from.' - '.$to?></td>
                                                        <td><?php echo $item['instructor']['emp_fname'].' '.$item['instructor']['emp_mname'].' '.$item['instructor']['emp_lname']; ?></td>
                                                        <td>
                                                            <a href="delete_lecture.php?i=<?php echo $_GET['i']?>&l=<?php echo $item['lecture']['id']?>" class="btn btn-danger">DELETE</a>
                                                            <a href="modify_lecture.php?i=<?php echo $_GET['i']?>&l=<?php echo $item['lecture']['id']?>" class="btn btn-warning">MODIFY</a>
                                                        </td>
                                                    </tr>
                                                    <?
                                                }
                                            }?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?
                                }
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
