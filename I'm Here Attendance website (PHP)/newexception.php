<?php
if(!isset($_GET['i']) || empty($_GET['i'])){
    header('location: semesters.php');
}
include "header.php";
$pre = 'core/';
include_once ('core/ExpFunctions.php');
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section pb-0">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/semester-details.php?i=<?php echo $_GET['i']?>"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 0">Exception Dates</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <form method="post" action="controller/exception/add.php">
                        <div class="form-row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input type="date" class="form-control" name="date" id="date" placeholder="Date" required>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <input type="text" class="form-control" name="description" id="description" placeholder="Description" required>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 32px;">
                                <div class="form-group text-center">
                                    <input type="hidden" name="semester" value="<?php echo $_GET['i']?>">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add Exception</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 p-0">
                        <?php if(isset($_SESSION['message_session'])){
                            if ($_SESSION['message_session']['status']){?>
                                <div class="col-md-12 alert alert-success">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}else{?>
                                <div class="col-md-12 alert alert-danger">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}
                        }?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $result = DisplayAllExps($_GET['i']);
                        if(!empty($result)) {
                            foreach ($result as $item) {
                                ?>
                                <tr>
                                    <td><?php echo $item['exp']->format('d/m/Y'); ?></td>
                                    <td><?php echo $item['description']; ?></td>
                                </tr>
                                <?
                            }
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
unset($_SESSION['message_session']);
include "footer.php"
?>
