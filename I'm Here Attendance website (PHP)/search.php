<?php
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/SerFunctions.php");

$result = null;
if(isset($_GET['search_key']) && !empty($_GET['search_key']) && isset($_GET['filter']) && !empty($_GET['filter'])){
    if($_GET['filter'] == 'in'){
        $result = getInstructorData($_GET['search_key']);
    }elseif ($_GET['filter'] == 'crn'){
        $result = getCRNData($_GET['search_key']);
    }elseif ($_GET['filter'] == 'st'){
        $result = getStudentData($_GET['search_key']);
    }
}
include "header.php";
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3>Search Results for: <span><?php echo $_GET['search_key']?></span></h3>
                </div>
            </div>
            <?php
            if(!empty($_GET['filter'] == "in")) {?>
            <div class="container">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Instructor Name</th>
                            <th>Course Name</th>
                            <th>Group Number</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($result)) {
                            foreach ($result as $item) {
                                ?>
                                <tr>
                                    <td><?php echo $item['instructor']['emp_id']; ?></td>
                                    <td><?php echo $item['instructor']['emp_fname'].' '.$item['instructor']['emp_mname'].' '.$item['instructor']['emp_lname']; ?></td>
                                    <td><a><?php echo $item['course']['name']; ?></a></td>
                                    <td><?php echo $item['group']['name']; ?></td>
                                </tr>
                                <?
                            }
                        }else{?>
                            <tr>
                                <td colspan="4">
                                    <div class="col-md-12 alert alert-danger mb-0">
                                        No Result Found
                                    </div>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php }?>
            <?php
            if(!empty($_GET['filter'] == "crn")) {?>
            <div class="container">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>CRN</th>
                            <th>Course Name</th>
                            <th>Instructor Name</th>
                            <th>Type</th>
                            <th>Group Number</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($result)) {
                            foreach ($result as $item) {
                                ?>
                                <tr>
                                    <td><?php echo $item['course']['crn']; ?></td>
                                    <td><a><?php echo $item['course']['name']; ?></a></td>
                                    <td><?php echo $item['instructor']['emp_fname'].' '.$item['instructor']['emp_mname'].' '.$item['instructor']['emp_lname']; ?></td>
                                    <td><?php echo $item['course']['activity']; ?></td>
                                    <td><?php echo $item['group']['name']; ?></td>
                                </tr>
                                <?
                            }
                        }else{?>
                            <tr>
                                <td colspan="5">
                                    <div class="col-md-12 alert alert-danger mb-0">
                                        No Result Found
                                    </div>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php }?>
            <?php
            if(!empty($_GET['filter'] == "st")) {?>
            <div class="container">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Student Name</th>
                            <th>Course Name</th>
                            <th>Group Number</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(!empty($result)) {
                            foreach ($result as $item) {
                                ?>
                                <tr>
                                    <td><?php echo $item['student']['studentid']; ?></td>
                                    <td><?php echo $item['student']['studentfname'].' '.$item['student']['studentmname'].' '.$item['student']['studentlname']; ?></td>
                                    <td><a><?php echo $item['course']['name']; ?></a></td>
                                    <td><?php echo $item['group']['name']; ?></td>
                                </tr>
                                <?
                            }
                        }else{?>
                            <tr>
                                <td colspan="4">
                                    <div class="col-md-12 alert alert-danger mb-0">
                                        No Result Found
                                    </div>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</section>
<?php include_once ('footer.php');?>