<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/AccFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">User Account</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="newStudentAccount.php" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Student Account</a>
                        </div>
                        <div class="col-md-12">
                            <a href="newInstructorAccount.php" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Instructor Account</a>
                        </div>
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                $result = DisplayAllusersaccounts();
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        $status = "Enable";
                                        $link = '<a href="dis-account.php?i=' . $item['id'] . '" class="btn btn-danger">Disable</a>';
                                        if (!$item['enable_disable']) {
                                            $status = "Disable";
                                            $link = '<a href="en-account.php?i=' . $item['id'] . '" class="btn btn-success">Enable</a>';
                                        } ?>
                                        <tr>
                                            <td><?php echo $item['username']; ?></td>
                                            <td><?php echo $item['type']; ?></td>
                                            <td><?php echo $status; ?></td>
                                            <td><?php echo $link; ?></td>
                                        </tr>
                                    <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <?php if(isset($_SESSION['message_session'])){
                                if ($_SESSION['message_session']['status']){?>
                                    <div class="col-md-12 alert alert-success">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}else{?>
                                    <div class="col-md-12 alert alert-danger">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
