<?php
if(!isset($_GET['g']) || empty($_GET['g'])){
    header('location: mngGroup.php');
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/GrpFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="mngGroup.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Group Students</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="newGroupStudent.php?g=<?php echo $_GET['g']?>" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Student to Group</a>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Student ID</th>
                                    <th>Student Name</th>
                                    <th>Student Gender</th>
                                    <th>Student Phone</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $result = getStudentsByGroupId($_GET['g']);
                                if(!empty($result)) {
                                    foreach ($result as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item['student']['studentid']; ?></td>
                                            <td><?php echo $item['student']['studentfname'].' '.$item['student']['studentmname'].' '.$item['student']['studentlname'];?></td>
                                            <td><?php echo $item['student']['studentgender']; ?></td>
                                            <td><?php echo $item['student']['studentphone']; ?></td>
                                            <td><a href="delete_group_student.php?g=<?php echo $_GET['g']?>&r=<?php echo $item['relation']['id']?>" class="btn btn-danger">DELETE</a></td>
                                        </tr>
                                    <?
                                    }
                                }?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 p-0">
                            <?php if(isset($_SESSION['message_session'])){
                                if ($_SESSION['message_session']['status']){?>
                                    <div class="col-md-12 alert alert-success">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}else{?>
                                    <div class="col-md-12 alert alert-danger">
                                        <?php echo $_SESSION['message_session']['message']?>
                                    </div>
                                <?}
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
unset($_SESSION['message_session']);
include "footer.php"
?>
