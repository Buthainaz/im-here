<?php
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/EmpFunctions.php");
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Employees</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 p-0">
                            <a href="newEmployee.php" class="btn btn-success float-right mb-2"><i class="fa fa-plus"></i> Add New Employee</a>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Office No</th>
                                    <th>Coordinator</th>
                                    <th>Rank</th>
                                    <th>Department</th>
                                    <th>Type</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?
                                foreach (DisplayAllEmployees() as $item){?>
                                <tr>
                                    <td><?php echo $item['emp_fname'].' '.$item['emp_mname'].' '.$item['emp_lname'];?></td>
                                    <td><?php echo $item['emp_email'];?></td>
                                    <td><?php echo $item['emp_phone'];?></td>
                                    <td><?php echo $item['emp_officeno'];?></td>
                                    <td><?php echo $item['emp_coordinator'];?></td>
                                    <td><?php echo $item['emp_rank'];?></td>
                                    <td><?php echo $item['emp_dep'];?></td>
                                    <td><b class="text-danger"><?php echo $item['type'];?></b></td>
                                </tr>
                                <?}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
