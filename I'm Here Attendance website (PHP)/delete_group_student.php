<?php
if(!isset($_GET['g']) || empty($_GET['g'])){
    header('location: mngGroup.php');
}
if(!isset($_GET['r']) || empty($_GET['r'])){
    header('location: mngGroupStudent.php?g='.$_GET['r']);
}
include "header.php";
$pre = 'core/';
include_once ("core/engine.php");
include_once ("core/GrpFunctions.php");

$result = getGroupById(intval($_GET['g']));
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="mngGroup.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 30px">Delete Student From Group</h3>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="controller/group/delete_student.php">
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <p class="text-center">Are you sure you want to delete current student from "<?php echo $result['name']?>" group ?</p>
                                        <div class="form-group text-center">
                                            <input type="hidden" name="g" id="g" value="<?php echo $result['id']?>" required>
                                            <input type="hidden" name="r" id="r" value="<?php echo $_GET['r']?>" required>
                                            <button type="submit" class="btn btn-danger">Yes</button>
                                            <a href="mngGroup.php" class="btn btn-warning">No</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include "footer.php"
?>
