<?php
include "header.php";
$pre = 'core/';
include_once ('core/AccFunctions.php');
?>
<section class="container-fluid">
    <div class="row">
        <div class="col-md-12 search-section pb-0">
            <div class="row">
                <div class="col-md-12 back-button">
                    <a href="/I'mHere/mngAccount.php"><h4><i class="fa fa-chevron-left"></i> Back</h4></a>
                </div>
                <div class="col-md-12">
                    <h3 style="margin-bottom: 0">Add New Student Account</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12 semester-section">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form method="post" action="controller/account/add_student.php">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="record">Student</label>
                                    <select class="form-control" name="record" id="record" required>
                                        <?php
                                        $result = DisplayAllStudentWithoutAccounts();
                                        if(!empty($result)) {
                                            foreach ($result as $item) {
                                                ?>
                                                    <option value="<?php echo $item['studentid'];?>"><?php echo $item['studentfname'].' '.$item['studentmname'].' '.$item['studentlname']; ?></option>
                                                <?
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="confirm-password">Confirm Password</label>
                                    <input type="password" class="form-control" name="confirm-password" id="confirm-password" placeholder="Confirm Password" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Add New Account</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 p-0">
                        <?php if(isset($_SESSION['message_session'])){
                            if ($_SESSION['message_session']['status']){?>
                                <div class="col-md-12 alert alert-success">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}else{?>
                                <div class="col-md-12 alert alert-danger">
                                    <?php echo $_SESSION['message_session']['message']?>
                                </div>
                            <?}
                        }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
unset($_SESSION['message_session']);
include "footer.php"
?>
