<?php unset($_SESSION['message_session'])?>
<footer class="footer">
    <div class="col-md-12">                                 
        <p>&copy;2020 I'm Here Team, All Rights Reserved</p>
    </div>
</footer>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/js/jquery-3.3.1.slim.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>