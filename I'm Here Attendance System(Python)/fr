import face_recognition
import cv2
import numpy as np
import platform
font = cv2.FONT_HERSHEY_DUPLEX
from livenessmodel import get_liveness_model
from imgdir import get_student
import tensorflow as tf
from matching import check_matching
import mysql.connector

#gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=1)
#sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

model= get_liveness_model()
model.load('model/my_model.tflearn')
print("Liveness Model is ready!")

known_names, known_encods = get_student()

def running_on_jetson_nano():
    # To make the same code work on a laptop or on a Jetson Nano, we'll detect when we are running on the Nano
    # so that we can access the camera correctly in that case.
    # On a normal Intel laptop, platform.machine() will be "x86_64" instead of "aarch64"
    return platform.machine() == "aarch64"

def get_jetson_gstreamer_source(capture_width=1280, capture_height=720, display_width=1280, display_height=720, framerate=60, flip_method=0):
    """
    Return an OpenCV-compatible video source description that uses gstreamer to capture video from the camera on a Jetson Nano
    """
    return (
            f'nvarguscamerasrc ! video/x-raw(memory:NVMM), ' +
            f'width=(int){capture_width}, height=(int){capture_height}, ' +
            f'format=(string)NV12, framerate=(fraction){framerate}/1 ! ' +
            f'nvvidconv flip-method={flip_method} ! ' +
            f'video/x-raw, width=(int){display_width}, height=(int){display_height}, format=(string)BGRx ! ' +
            'videoconvert ! video/x-raw, format=(string)BGR ! appsink'

            )

def main_loop():
    # Get access to the webcam. The method is different depending on if this is running on a laptop or a Jetson Nano.
    global process_this_frame
    if running_on_jetson_nano():
        # Accessing the camera with OpenCV on a Jetson Nano requires gstreamer with a custom gstreamer source string
        video_capture = cv2.VideoCapture(get_jetson_gstreamer_source(), cv2.CAP_GSTREAMER)

        # Initialize some variables
        face_locations = []
        face_encodings = []
        face_names = []
        process_this_frame = True
    else:
        # Accessing the camera with Opencv on a normal Desktop like Windows or Mac OS
        video_capture = cv2.VideoCapture(1)

        # Initialize some variables
        face_locations = []
        face_encodings = []
        face_names = []
        process_this_frame = True

    while True:
        # Grab a single frame of video
        ret, frame = video_capture.read()

        liveimg = cv2.resize(frame, (100, 100))
        liveimg = cv2.cvtColor(liveimg, cv2.COLOR_BGR2GRAY)
        liveimg = np.array([liveimg / 255])
        liveimg = liveimg.reshape((-1, 100, 100, 1))
        pred = model.predict(liveimg)

        if pred[0][0] > .75:

            # Resize frame of video to 1/4 size for faster face recognition processing
            small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

            # Only process every other frame of video to save time
            if process_this_frame:
                # Find all the faces and face encodings in the current frame of video
                face_locations = face_recognition.face_locations(small_frame)
                face_encodings = face_recognition.face_encodings(small_frame, face_locations)
                name = "Unknown"
                face_names = []
                for face_encoding in face_encodings:
                    for ii in range(len(known_encods)):
                        # See if the face is a match for the known face(s)
                        match = face_recognition.compare_faces([known_encods[ii]], face_encoding)

                        if match[0]:
                            name = known_names[ii]

                    face_names.append(name)

            process_this_frame = not process_this_frame

            unlock = False
            for n in face_names:

                if n != 'Unknown':
                    unlock = True

            # Display the results
            for (top, right, bottom, left), name in zip(face_locations, face_names):
                # Scale back up face locations since the frame we detected in was scaled to 1/4 size
                top *= 4
                right *= 4
                bottom *= 4
                left *= 4

                # Draw a box around the face
                cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

                # Draw a label with a name below the face
                cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)

                cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
                if unlock:
                    cv2.putText(frame, 'REAL STUDENT', (frame.shape[1] // 2, frame.shape[0] // 2), font, 1.0, (255, 255, 255),
                                1)
                else:
                    cv2.putText(frame, 'UNKOWN PERSON!', (frame.shape[1] // 2, frame.shape[0] // 2), font, 1.0,
                                (255, 255, 255), 1)



        else:
            cv2.putText(frame, 'UNREAL STUDENT!', (frame.shape[1] // 2, frame.shape[0] // 2), font, 1.0, (255, 255, 255), 1)
            # Display the resulting image
        cv2.imshow('Video', frame)

        # Hit 'q' on the keyboard to quit!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release handle to the webcam
    video_capture.release()
    cv2.destroyAllWindows()
    check_matching(name)



if __name__ == "__main__":
    main_loop()
